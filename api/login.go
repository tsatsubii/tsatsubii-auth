package api

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/ken109/gin-jwt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
	"gitlab.com/tsatsubii/tsatsubii-auth/models"
)

func Login(c *gin.Context) {
	var form forms.LoginForm

	if err := c.ShouldBindJSON(&form); err != nil {
		log.Error(err)
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	repo := models.UserRepository{}
	user := repo.Authenticate(form)
	if user.ID != uuid.Nil {
		token, refreshToken := user.IssueToken()
		c.JSON(
			http.StatusOK,
			gin.H{
				"token":         token,
				"refresh_token": refreshToken,
			},
		)

		return
	}

	c.JSON(
		http.StatusForbidden,
		gin.H{
			"error": "authentication failed",
		},
	)
}

func Sys(c *gin.Context) {
	var form tsatsubii.SystemForm

	if err := c.ShouldBindJSON(&form); err != nil {
		log.Error(err)
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	if form.Secret != tsatsubii.SystemSecret {
		c.JSON(http.StatusForbidden, "access denied")
		return
	}

	token, refreshToken, _ := jwt.IssueToken(
		os.Getenv("JWT_REALM"),
		jwt.Claims{
			"role_id": tsatsubii.SystemRoleID(),
		},
	)

	c.JSON(
		http.StatusOK,
		gin.H{
			"token":         token,
			"refresh_token": refreshToken,
		},
	)
}
