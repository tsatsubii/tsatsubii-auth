package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
	"gitlab.com/tsatsubii/tsatsubii-auth/models"
)

func GetUsers(c *gin.Context) {
	userRepo := models.UserRepository{}
	users := userRepo.FindAll()

	c.JSON(
		http.StatusOK,
		gin.H{
			"status":  "success",
			"message": "",
			"data":    users,
		},
	)
}

func CreateUser(c *gin.Context) {
	var form forms.UserForm

	if err := c.ShouldBindJSON(&form); err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	userRepo := models.UserRepository{}
	user, err := userRepo.Create(form)
	if err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "success",
			"user_id": user.ID,
		},
	)
}

func UpdateUser(c *gin.Context) {
	var form forms.UserForm

	if err := c.ShouldBindJSON(&form); err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	userRepo := models.UserRepository{}
	user := userRepo.Get(form.ID)

	err := user.Update(form)
	if err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"message": "success",
			"user_id": user.ID,
		},
	)
}
