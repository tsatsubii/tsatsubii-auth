package main

import (
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-auth/api"
	"gitlab.com/tsatsubii/tsatsubii-auth/models"
	"gitlab.com/tsatsubii/tsatsubii-auth/utils"
)

func main() {
	tsatsubii.EnsureEnv("SA_EMAIL")
	tsatsubii.EnsureEnv("SA_PASSWORD")

	tsatsubii.Init()
	models.Setup()
	utils.JWTSetup()

	tsatsubii.AddRoute("POST", "/login", api.Login)
	tsatsubii.AddRoute("POST", "/sys", api.Sys)

	tsatsubii.AddAuthRoute(
		"GET",
		"/users",
		models.PermManageUsers,
		api.GetUsers,
	)

	tsatsubii.AddAuthRoute(
		"POST",
		"/users",
		tsatsubii.PermNone,
		api.CreateUser,
	)

	tsatsubii.AddAuthRoute(
		"UPDATE",
		"/users",
		models.PermManageUsers,
		api.UpdateUser,
	)

	tsatsubii.Start()
}
