package forms

import (
	"github.com/google/uuid"
)

type UserForm struct {
	ID          uuid.UUID `json:"id"`
	Email       string    `json:"email" binding:"required"`
	Password    string    `json:"password"`
	Role        string    `json:"role" binding:"required"`
	DisplayName string    `json:"display_name"`
}
