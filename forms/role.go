package forms

import (
	"github.com/google/uuid"
)

type RoleForm struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name" binding:"required"`
}
