package utils

import (
	"os"

	"github.com/ken109/gin-jwt"
	log "github.com/sirupsen/logrus"
)

func JWTSetup() {
	if val, ok := os.LookupEnv("JWT_REALM"); !ok || val == "" {
		log.Fatal("JWT_REALM not set")
	}

	err := jwt.SetUp(
		jwt.Option{
			Realm:            os.Getenv("JWT_REALM"),
			SigningAlgorithm: jwt.RS256,
			PrivKeyFile:      "private.key",
		},
	)

	if err != nil {
		log.Fatal(err)
	}
}

/*
func IssueJWTToken(user models.User) (string, string) {
	token, refreshToken, err := jwt.IssueToken(
		os.Getenv("JWT_REALM"),
		jwt.Claims{
			"email": email,
		},
	)

	if err != nil {
		log.Error(err)
		return "", ""
	}

	return token, refreshToken
}
*/
