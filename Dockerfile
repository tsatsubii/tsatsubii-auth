FROM golang:alpine

RUN apk update && apk add --no-cache git openssl
RUN mkdir /app
WORKDIR /app

COPY . .
RUN go get
RUN go build
RUN openssl genrsa -out private.key 2048

CMD ["/app/tsatsubii-auth"]
