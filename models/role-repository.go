package models

import (
	"github.com/google/uuid"
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
)

type RoleRepository struct{}

func (r *RoleRepository) Get(id uuid.UUID) (Role, error) {
	var role Role

	result := Db.Model(
		&Role{},
	).Where(
		"id = ?",
		id,
	).First(
		&role,
	)

	if result.Error != nil {
		return Role{}, result.Error
	}

	return role, nil
}

func (r *RoleRepository) GetOrCreate(name string) (Role, error) {
	var role Role

	result := Db.Where(
		Role{
			Name: name,
		},
	).FirstOrCreate(
		&role,
	)

	if result.Error != nil {
		return Role{}, result.Error
	}

	return role, nil
}

func (r *RoleRepository) Create(form forms.RoleForm) (Role, error) {
	role := Role{
		Name: form.Name,
	}

	if err := role.Validate(); err != nil {
		return Role{}, err
	}

	if err := Db.Create(&role).Error; err != nil {
		err := tsatsubii.InternalError{}
		return Role{}, &err
	}

	return role, nil
}

func (r *RoleRepository) SysAdminRole() (Role, error) {
	role, err := r.Get(tsatsubii.SysAdminRoleID())
	if err == nil {
		return role, nil
	}

	role = Role{}
	role.ID = tsatsubii.SysAdminRoleID()
	role.Name = AdminRole

	if err := Db.Create(&role).Error; err != nil {
		return Role{}, err
	}

	return role, nil
}

func (r *RoleRepository) Exists(name string) bool {
	var count int64

	result := Db.Model(
		&Role{},
	).Where(
		"name = ?",
		name,
	).Count(
		&count,
	)

	if result.Error != nil {
		return false
	}

	return count > 0
}
