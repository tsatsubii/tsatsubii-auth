package models

import (
	"gitlab.com/tsatsubii/tsatsubii-service"
)

const (
	AdminRole string = "System Administrator"
)

type Role struct {
	tsatsubii.Model
	Name        string                `gorm:"unique" json:"name"`
	Permissions tsatsubii.Permissions `json:"permissions"`
	Users       Users                 `json:"users"`
}

type Roles []Role

func (r *Role) Validate() error {
	// TODO: implement this
	return nil
}

func (r *Role) HasPermission(key tsatsubii.PermissionKey) bool {
	return tsatsubii.HasPermission(r.ID, key)
}

func (r *Role) GrantPermission(key tsatsubii.PermissionKey) {
	tsatsubii.GrantPermission(r.ID, key)
}

func (r *Role) RevokePermission(key tsatsubii.PermissionKey) {
	tsatsubii.RevokePermission(r.ID, key)
}
