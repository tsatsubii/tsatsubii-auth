package models

import (
	"fmt"
	"net/mail"
	"os"

	"github.com/google/uuid"
	"github.com/ken109/gin-jwt"
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
)

type UserState string

const (
	UserNew     UserState = "new"
	UserActive  UserState = "active"
	UserDeleted UserState = "deleted"
)

type User struct {
	tsatsubii.Model
	Email       string    `json:"email" gorm:"unique"`
	Password    string    `json:"password"`
	DisplayName string    `json:"display_name"`
	RoleID      uuid.UUID `json:"role_id"`
	State       UserState `gorm:"default:'new'" json:"state"`
}

type Users []User

type UserSummary struct {
	ID          uuid.UUID `json:"id"`
	Email       string    `json:"email"`
	DisplayName string    `json:"display_name"`
	RoleID      uuid.UUID `json:"role_id"`
}

type UserSummaries []UserSummary

func (u *User) Validate() error {
	err := tsatsubii.ValidationError{}

	if !u.ValidateEmail() {
		err.AddError("invalid email address")
		//errs = append(errs, errors.New("invalid email address"))
	}

	repo := UserRepository{}
	if repo.Exists(u.Email, u.ID) {
		err.AddError("email address already exists")
		//errs = append(errs, errors.New("email already exists"))
	}

	if !u.ValidatePassword() {
		err.AddError("bad password")
		//errs = append(errs, errors.New("bad password"))
	}

	if err.HasErrors() {
		return &err
	}

	return nil
}

func (u *User) ValidateEmail() bool {
	email, err := mail.ParseAddress(u.Email)
	if err != nil {
		return false
	}

	if u.Email != email.Address {
		return false
	}

	return true
}

func (u *User) ValidatePassword() bool {
	// TODO: implement this
	return true
}

func (u *User) IssueToken() (string, string) {
	displayName := u.DisplayName
	if displayName == "" {
		displayName = u.Email
	}

	token, refreshToken, err := jwt.IssueToken(
		os.Getenv("JWT_REALM"),
		jwt.Claims{
			"user_id":      u.ID.String(),
			"display_name": u.DisplayName,
			"role_id":      u.RoleID.String(),
		},
	)

	if err != nil {
		fmt.Println(err)
		return "", ""
	}

	return token, refreshToken
}

func (u *User) Update(form forms.UserForm) error {
	roleRepo := RoleRepository{}
	role, _ := roleRepo.GetOrCreate(form.Role)

	u.Email = form.Email
	u.Password = form.Password
	u.RoleID = role.ID
	u.DisplayName = form.DisplayName

	if err := u.Validate(); err != nil {
		return err
	}

	if err := Db.Save(&u).Error; err != nil {
		err := tsatsubii.InternalError{}
		return &err
	}

	return nil
}
