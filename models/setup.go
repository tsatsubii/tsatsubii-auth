package models

import (
	"os"

	"gitlab.com/tsatsubii/tsatsubii-service"
	"gorm.io/gorm"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
)

const (
	PermManageUsers tsatsubii.PermissionKey = "manage users"
)

var Db *gorm.DB

func Setup() {
	Db = tsatsubii.Database()

	Db.AutoMigrate(&User{})
	Db.AutoMigrate(&Role{})

	roleRepo := RoleRepository{}
	saRole, _ := roleRepo.SysAdminRole()
	saRole.GrantPermission(PermManageUsers)

	userRepo := UserRepository{}
	userRepo.Create(forms.UserForm{
		Email:       os.Getenv("SA_EMAIL"),
		Password:    os.Getenv("SA_PASSWORD"),
		DisplayName: "System Administrator",
		Role:        saRole.Name,
	})
}
