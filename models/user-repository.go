package models

import (
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-auth/forms"
)

type UserRepository struct{}

func (r *UserRepository) Create(form forms.UserForm) (User, error) {
	roleRepo := RoleRepository{}
	role, _ := roleRepo.GetOrCreate(form.Role)

	user := User{
		Email:       form.Email,
		Password:    form.Password,
		RoleID:      role.ID,
		DisplayName: form.DisplayName,
	}

	if err := user.Validate(); err != nil {
		return User{}, err
	}

	if err := Db.Create(&user).Error; err != nil {
		log.Error(err)
		err := tsatsubii.InternalError{}
		return User{}, &err
	}

	return user, nil
}

func (r *UserRepository) Get(id uuid.UUID) User {
	var user User

	result := Db.Model(
		&User{},
	).Where(
		"id = ?",
		id,
	).First(
		&user,
	)

	if result.Error != nil {
		return User{}
	}

	return user
}

func (r *UserRepository) GetByEmail(email string) User {
	var user User

	result := Db.Model(
		&User{},
	).Select(
		"email, display_name, role_id, state",
	).Where(
		"email = ?",
		email,
	).First(
		&user,
	)

	if result.Error != nil {
		return User{}
	}

	return user
}

func (r *UserRepository) Exists(email string, exclude uuid.UUID) bool {
	var count int64

	result := Db.Model(
		&User{},
	).Where(
		"email = ? and id != ?",
		email,
		exclude,
	).Count(
		&count,
	)

	if result.Error != nil {
		log.Error(result.Error)
		return false
	}

	return count > 0
}

func (r *UserRepository) FindAll() UserSummaries {
	var users UserSummaries

	result := Db.Model(
		&User{},
	).Select(
		"id, email, display_name, role_id",
	).Where(
		"state != ?",
		UserDeleted,
	).Find(
		&users,
	)

	if result.Error != nil {
		return nil
	}

	return users
}

func (r *UserRepository) Count() int64 {
	var count int64

	result := Db.Model(
		&User{},
	).Where(
		"state != ?",
		UserDeleted,
	).Count(
		&count,
	)

	if result.Error != nil {
		return 0
	}

	return count
}

func (r *UserRepository) Authenticate(form forms.LoginForm) User {
	var user User

	result := Db.Model(
		&User{},
	).Where(
		"email = ? AND password = ?",
		form.Email,
		form.Password,
	).First(
		&user,
	)

	if result.Error != nil {
		return User{}
	}

	return user
}
